create database file_manager comment '文件管理系统数据库';

create table t_user
(
	id int auto_increment comment '用户Id',
	username varchar(30) not null comment '用户名称',
	password varchar(255) not null comment '用户密码',
	constraint t_user_pk
		primary key (id)
)
comment '用户表';

create table t_files
(
	id int auto_increment comment '文件Id',
	origin_name varchar(255) not null comment '文件原始名称',
	store_name varchar(255) not null comment '文件存储名称',
	extension varchar(24) not null comment '文件扩展名',
	store_path varchar(255) not null comment '文件存储路径',
	size int not null comment '文件大小，单位Kib',
	type varchar(32) not null comment '文件类型',
	download_count int default 0 not null comment '文件下载次数',
	upload_time datetime default now() not null comment '文件上传时间',
	constraint t_files_pk
		primary key (id)
)
comment '文件表';
