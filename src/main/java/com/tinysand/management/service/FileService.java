package com.tinysand.management.service;

import com.tinysand.management.entity.FileInfo;

import java.util.List;

/**
 * @author tiny
 */
public interface FileService {
    List<FileInfo> findFilesByUserId(Integer userId);

    void saveFileInfo(FileInfo file);

    FileInfo findFileById(Integer fileId);

    void updateFileInfo(FileInfo fileInfo);

    void deleteFileInfo(Integer fileId);
}
