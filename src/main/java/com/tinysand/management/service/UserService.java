package com.tinysand.management.service;

import com.tinysand.management.entity.User;

/**
 * @author tiny
 */
public interface UserService {
    User login(User user);
}
