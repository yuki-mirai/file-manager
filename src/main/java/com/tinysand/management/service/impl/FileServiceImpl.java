package com.tinysand.management.service.impl;

import com.tinysand.management.accessor.FileAccessor;
import com.tinysand.management.entity.FileInfo;
import com.tinysand.management.service.FileService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tiny
 */
@Service
@Transactional
public class FileServiceImpl implements FileService {
    private final FileAccessor fileAccessor;

    public FileServiceImpl(FileAccessor fileAccessor) {
        this.fileAccessor = fileAccessor;
    }

    @Override
    public List<FileInfo> findFilesByUserId(Integer userId) {
        return fileAccessor.findFilesByUserId(userId);
    }

    @Override
    public void saveFileInfo(FileInfo file) {
        fileAccessor.saveFileInfo(file);
    }

    @Override
    public FileInfo findFileById(Integer fileId) {
        return fileAccessor.findFileById(fileId);
    }

    @Override
    public void updateFileInfo(FileInfo fileInfo) {
        fileAccessor.updateFileInfo(fileInfo);
    }

    @Override
    public void deleteFileInfo(Integer fileId) {
        fileAccessor.deleteFileInfo(fileId);
    }
}
