package com.tinysand.management.service.impl;

import com.tinysand.management.accessor.UserAccessor;
import com.tinysand.management.entity.User;
import com.tinysand.management.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author tiny
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final UserAccessor userAccessor;

    public UserServiceImpl(UserAccessor userAccessor) {
        this.userAccessor = userAccessor;
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public User login(User user) {
        return userAccessor.findUser(user.getUsername());
    }
}
