package com.tinysand.management.accessor;

import com.tinysand.management.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author tiny
 */
@Repository
public interface UserAccessor {
    User findUser(String username); // 根据用户名查找用户
}
