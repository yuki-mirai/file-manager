package com.tinysand.management.accessor;

import com.tinysand.management.entity.FileInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author tiny
 */
@Repository
public interface FileAccessor {
    List<FileInfo> findFilesByUserId(Integer userId); // 根据用户Id查询所有用户文件

    void saveFileInfo(FileInfo file); // 保存用户上传文件的信息

    FileInfo findFileById(Integer fileId); // 根据文件id获取文件信息对象

    void updateFileInfo(FileInfo fileInfo); // 更新文件信息

    void deleteFileInfo(Integer fileId); // 根据id删除文件信息
}
