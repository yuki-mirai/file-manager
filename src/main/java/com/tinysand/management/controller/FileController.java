package com.tinysand.management.controller;

import com.tinysand.management.entity.FileInfo;
import com.tinysand.management.entity.User;
import com.tinysand.management.service.FileService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @author tiny
 */
@Controller
@RequestMapping("file")
public class FileController {
    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/{id}")
    public void downloadFile(@PathVariable Integer id,
                             @RequestParam("type") String operationType,
                             HttpServletResponse httpServletResponse)
            throws IOException {
        FileInfo fileInfo = fileService.findFileById(id);

        InputStream fileInputStream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(String.format("static%s/%s",
                        fileInfo.getStorePath(), fileInfo.getStoreName()));

        String contentDisposition = String.format("%s;fileName=%s", operationType,
                URLEncoder.encode(fileInfo.getOriginName(), "UTF-8"));

        httpServletResponse.setHeader("content-disposition", contentDisposition);
        ServletOutputStream outputStream = httpServletResponse.getOutputStream();
        if (fileInputStream != null) {
            IOUtils.copy(fileInputStream, outputStream);
            fileInputStream.close();
            outputStream.close();
        }
        if ("attachment".equalsIgnoreCase(operationType)) {
            fileInfo.setDownloadCount(fileInfo.getDownloadCount() + 1);
            fileService.updateFileInfo(fileInfo);
        }
    }

    /**
     * 处理用户上传文件请求
     *
     * @param file 用户表单提交的文件数据
     * @return 文件上传成功后返回文件列表视图
     */
    @PostMapping
    public String uploadFile(MultipartFile file, HttpSession httpSession)
            throws IOException {
        String originalFileName = file.getOriginalFilename();

        // 获取文件的扩展名
        String extension = FilenameUtils.getExtension(originalFileName);

        String newFileName = String.format("%s.%s",
                UUID.randomUUID().toString(),
                FilenameUtils.getExtension(originalFileName));

        long kibSize = file.getSize() / 1024; // 文件大小

        String basePath = ResourceUtils.getURL("classpath:").getPath()
                + "/static/uploads";
//        String dateFormatDir = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        SimpleDateFormat dateDirFormat = new SimpleDateFormat("yyyy-MM-dd");
        File dateFormatDir = new File(basePath + "/" +
                dateDirFormat.format(new Date()));
        boolean success = true;
        if (!dateFormatDir.exists()) {
            success = dateFormatDir.mkdirs();
        }
        if (success) {
            file.transferTo(new File(dateFormatDir, newFileName));
            User user = (User) httpSession.getAttribute("principal"); // 取得当前登录用户
            FileInfo fileInfo = new FileInfo()
                    .setOriginName(originalFileName).setStoreName(newFileName)
                    .setExtension(extension).setStorePath("/uploads/"
                            + dateDirFormat.format(new Date()))
                    .setSize(kibSize).setUploadTime(new Date())
                    .setDownloadCount(0).setUserId(user.getId())
                    .setType(file.getContentType());

            fileService.saveFileInfo(fileInfo);
        }
        return "redirect:/file";
    }
    /**
     * 列出所有文件的详细信息
     *
     * @return 请求处理完毕后跳转到文件详情页
     */
    @GetMapping
    public String listFilesInfo(HttpSession httpSession, Model model) {
        User principal = (User) httpSession.getAttribute("principal");
        model.addAttribute("files", fileService
                .findFilesByUserId(principal.getId()));
        return "details";
    }

    @DeleteMapping("/{id}")
    public String deleteFile(@PathVariable("id") Integer id)
            throws FileNotFoundException {
        FileInfo fileInfo = fileService.findFileById(id);
        String realPath = ResourceUtils.getURL("classpath:").getPath()
                + "/static" + fileInfo.getStorePath();

        File file = new File(realPath, fileInfo.getStoreName());
        if (file.exists()) {
            if (file.delete()) fileService.deleteFileInfo(fileInfo.getId());
        }
        return "redirect:/file";
    }
}
