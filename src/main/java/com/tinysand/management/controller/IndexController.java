package com.tinysand.management.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author tiny
 */
@Controller
public class IndexController {

    /**
     * 处理登录页面跳转请求
     *
     * @return 登录页面
     */
    @GetMapping("login")
    public String toLogin() { return "login"; }
}
