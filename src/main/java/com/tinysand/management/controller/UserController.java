package com.tinysand.management.controller;

import com.tinysand.management.entity.User;
import com.tinysand.management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * @author tiny
 */
@Controller
@RequestMapping("user")
public class UserController {
    private final UserService userService; // 用户相关操作服务

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 处理用户登录请求
     *
     * @param user 用户登录数据，用于查询数据库中存在的用户
     * @param httpSession 当查询到数据库中存在指定用户之后将其保存
     * @return 登录处理后的跳转路径
     */
    @PostMapping
    public String login(User user, HttpSession httpSession) {
        User principal = userService.login(user); // 根据前端用户数据从数据库中查询对应用户
        if (principal != null) {
            httpSession.setAttribute("principal", principal); // 将查询到的用户放到session
            return "redirect:/file";
        } else {
            return "redirect:/login"; // 查询不到指定用户
        }
    }
}
