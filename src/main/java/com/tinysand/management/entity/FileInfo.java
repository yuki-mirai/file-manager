package com.tinysand.management.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author tiny
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
public class FileInfo {
    private Integer id;               // 文件id
    private String originName;        // 文件原始名称
    private String storeName;         // 文件存储名
    private String extension;         // 文件扩展名
    private String storePath;         // 文件存储路径
    private Long size;                // 文件大小，单位Kib
    private String type;              // 文件类型
    private Integer downloadCount;    // 文件下载次数
    private Date uploadTime;          // 文件上传时间
    private Integer userId;           // 用户Id关联外键
}
